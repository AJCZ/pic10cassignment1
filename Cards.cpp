#include "Cards.h"

std::string card::get_rank() const{
	std::string str_rank = "";
	switch (rank)
	{
	case As: str_rank = "As";    break;
	case Dos: str_rank = "Dos"; break;
	case Tres: str_rank = "Tres";  break;
	case Cuatro: str_rank = "Cuatro";   break;
	case Cinco: str_rank = "Cinco"; break;
	case Seis: str_rank = "Seis"; break;
	case Siete: str_rank = "Siete"; break;
	case Sota: str_rank = "Sota"; break;
	case Caballo: str_rank = "Caballo"; break;
	case Rey: str_rank = "Rey"; break;
	default:str_rank = "";
	}
	return str_rank;
}

std::string card::get_rank_en() const {
	std::string str_rank = "";
	Rank_English rank_en = (Rank_English)rank;
	switch (rank_en)
	{
		case One: str_rank = "One";    break;
		case Two: str_rank ="Two"; break;
		case Three: str_rank= "Three";  break;
		case Four: str_rank="Four";   break;
		case Five: str_rank= "Five"; break;
		case Six: str_rank =  "Six"; break;
		case Seven: str_rank =  "Seven"; break;
		case Jack: str_rank =  "Jack"; break;
		case Knight: str_rank =  "Knight"; break;
		case King: str_rank =  "King"; break;
		default: str_rank = "";
	}
	return str_rank;
}

std::string card::get_suit() const {
	std::string str_suit = "";
	switch (suit) {
	case bastos: str_suit =  "bastos";    break;
	case oros: str_suit =  "oros"; break;
	case copas: str_suit =  "copas";  break;
	case espadas: str_suit =  "espadas";   break;
	default:str_suit = "";
	}
	return str_suit;
}

std::string card::get_suit_en() const {
	std::string str_suit = "";
	Suit_English suit_en = (Suit_English)suit;
	switch (suit_en) {
	case clubs: str_suit =  "clubs";    break;
	case golds: str_suit =  "golds"; break;
	case cups: str_suit =  "cups";  break;
	case swords: str_suit =  "swords";   break;
	default:str_suit = "";
	}
	return str_suit;
}

std::ostream & operator<<(std::ostream &os, const card& c)
{
	std::string str1 = c.get_rank() + " de " + c.get_suit();
	std::string str2 = "(" + c.get_rank_en() + " of " + c.get_suit_en() +").";
	return os << std::right << std::setw(7) << " " << std::left << std::setw(25) << str1 << str2;
}

card deck::get_next()
{
	card c = card_pile[order[current]];
	current++;
	if (current >= 40) {
		current = 0;
		shuffle();
	}
	return c;
}


std::ostream & operator<<(std::ostream &os, const Hand& hand)
{
	for (card c : hand.card_vector) {
		os << c << std::endl;
	}
	return os;
}
