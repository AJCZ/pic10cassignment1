#ifndef CARD_H
#define CARD_H
#include <iostream>
#include <string>
#include <iomanip>
#include <algorithm>
#include <cstdlib>
#include <vector>

/*
This enumeration is the set of ranks in Spanish.
*/
enum Rank {
	Rank_None,
	As,
	Dos,
	Tres,
	Cuatro,
	Cinco,
	Seis,
	Siete,
	Sota,
	Caballo,
	Rey
};

/*
This enumeration is the set of ranks in English.
*/
enum Rank_English {
	Rank_English_None,
	One,
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Jack,
	Knight,
	King
};

/*
This enumeration is the set of suit in Spanish.
*/
enum Suit {
	Suit_None,
	bastos,
	oros,
	copas,
	espadas
};

/*
This enumeration is the set of ranks in English.
*/
enum Suit_English {
	Suit_English_None,
	clubs,
	golds,
	cups,
	swords

};

class Deck;
class Hand;


/*
This class is the card. It has private fields rank, suit, and value.
*/
class card {
public:
	friend class Deck;
	friend class Hand;
	card(const Rank rr = Rank_None, const Suit ss = Suit_None) : rank(rr), suit(ss), value(0) {
		int int_rank = (int)rr;
		if (rr >= 8) {
			value = 0.5;
		}
		else {
			value = int_rank;
		}
	}

	// These getters return the string of suits/ranks in Spanish/English.
	std::string get_rank() const; 
	std::string get_rank_en() const;
	std::string get_suit() const;
	std::string get_suit_en() const;

	// returns the value of the card.
	double get_value() const { return value; }

	// output operator
	friend std::ostream & operator<<(std::ostream &os, const card& c); 

private:
	Rank rank;
	Suit suit;
	double value;

};



/*
Deck class. Can return the next card to be given to the players. Can shuffle.
*/

class deck {
public:
	deck():current(0) {
		for (int i = 1; i <= 10; i++) {
			for (int j = 1; j <= 4; j++) {
				card c = card((Rank)i, (Suit)j);
				int v = 4 * (i - 1) + j - 1;
				card_pile[v] = c;
				order[v] = v;
			}
		}
		std::srand(std::time(0));

	}

	void shuffle() {
		current = 0;
		std::random_shuffle(&order[0], &order[40]);
		return;
	}
	card get_next();
private:
	int current = 0;
	int order[40];
	card card_pile[40];
};

/*
class hand collects the cards belonging to the players.
*/
class Hand {
public:
	friend std::ostream & operator<<(std::ostream &os, const Hand& hand);
	double get_value() const {
		double value = 0.0;
		for (card s : card_vector) {
			value += s.get_value();
		}
		return value;
	}

	void add_card(card c) {
		card_vector.push_back(c);
	}
private:
	std::vector<card> card_vector;
};

#endif