#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <sstream>
#include "Cards.h"

using namespace std;


int play_one_round(deck&, int, double,int, int, ofstream&);

/*
This function lets the players play one round of the game. 
mydeck is the deck of cards.
bet is the player's bet.
goal is the winning value, in this case 7.5.
game_count is the number of count.
money_left is the money of the player.
f is the output log file.
*/
int play_one_round(deck& mydeck, int bet, double goal, int game_count, int money_left, ofstream& f) {
	int result = 0; // 0 for draw, 1 for player's win, -1 for dealer's win.

	mydeck.shuffle();
	Hand my_hand;
	Hand dealers_hand;
	my_hand.add_card(mydeck.get_next());	
	dealers_hand.add_card(mydeck.get_next());
	double my_value = 0.0;
	double dealers_value = 0.0;
	bool want_more = true;
	bool valid_input = true;

	/*
	Keep looping if the player wants one more card.
	*/
	while (want_more) {
		my_value = my_hand.get_value();
		dealers_value = dealers_hand.get_value();
		cout << "Your cards:" << endl;
		cout << my_hand;
		cout << "Your total is " << my_value << ". ";
		valid_input = true;
			
		/*
		If player already busted, break the loop. If player hits 7.5, break the loop.
		*/
		if (my_value >= 7.5) {
			want_more = false;
			if (my_value > 7.5) 
				cout << "You busted." << endl;
			continue;
		}
		cout << "Do you want another card? (y/n)";
		string s;
		getline(cin, s);
		
		if (s == "y") {
			card new_card = mydeck.get_next();
			my_hand.add_card(new_card);
			cout << "New card:" << endl << new_card << endl;
		}
		else if (s == "n") {
			want_more = false;
		}
		else {
			cout << "Invalid input. Try again." << endl;
			valid_input = false;
		}

		/*
		* If input not valid, ask for a valid input.
		*/
		if (valid_input) {

			// when the player does not bust (I let the computer cheats to add difficulty.) and the dealers value
			// is far from busting, keep adding cards.
			while ((my_value <= 7.5 && dealers_value <= 4.5)) {
				if (want_more == false && dealers_value > my_value) break;
				card new_card = mydeck.get_next();
				dealers_hand.add_card(new_card);
				dealers_value = dealers_hand.get_value();
			}
		}

	}

	/*
	update log file.
	*/
	f << "-----------------------------------------------" << endl << endl;
	f << "Game number:" << left << setw(9) << game_count << "Money left: " << money_left << endl;
	f << "Bet: " << bet << endl << endl;
	f << "You cards: " << endl;
	f << my_hand;
	f << "Your total: " << my_value << "." << endl << endl;

	cout << "Dealer's cards: " << endl;
	cout << dealers_hand;
	cout << "The dealer's total is " << dealers_value << "." << endl << endl;

	f << "Dealer's cards: " << endl;
	f << dealers_hand;
	f << "The dealer's total is " << dealers_value << "." << endl << endl;

	/*
	player loses.
	*/
	if (my_value > 7.5) {
		result = -1;
	}
	else {
		/*
		player wins.
		*/
		if (dealers_value > 7.5) {
			result = 1;
		}
		else {
			if (my_value == dealers_value) result = 0; // draw
			else if (abs(my_value - goal) < abs(dealers_value - goal)) result = 1; // player wins
			else result = -1; // player loses
		}
	}

	if (result == 1) {
		cout << "You win " << bet << "." << endl;
	}
	else if (result == -1) {
		cout << "Too bad. You lose " << bet << "." << endl;
	}
	else {
		cout << "Draw!" << endl;
	}

	

	return result;
}




int main() {





	






	deck mydeck = deck();
	double goal = 7.5; // initialize our goal.
	int game_count = 0;
	ofstream f;
	f.open("gamelog.txt"); // the output log file.
	bool next_round = true;
	int money = 100;

	// If the condition for starting the next round holds, continue.
	while (next_round) {
		string input = "";
		int bet;
		cout << "You have " << money << "." << "Enter bet:" ;
		getline(cin, input);
		stringstream myStream(input);
		if (myStream >> bet && bet <= money) {

			int result = play_one_round(mydeck, bet, goal, game_count, money, f);
			money += result * bet;
			game_count += 1;
		}

		/*
		* If input not valid, ask for a valid input.
		*/
		else {
			cout << "Invalid number, please try again" << endl;
		}
		if (money <= 0 || money >= 1000) next_round = false;
	}
	
	if (money <= 0) {
		cout << "You have 0. GAME OVER!" << endl;
		cout << "Come back when you have more money. " << endl << endl;
	}
	else {
		cout << "The dealer has no money!" << endl;
		cout << "Come back when the dealer has more money." << endl << endl;
		cout << "You win!" << endl << endl;
	}
	cout << "Bye!" << endl;
	f << "-----------------------------------------------";
	f.close();

	return 0;
}

	